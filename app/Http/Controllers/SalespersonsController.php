<?php

namespace App\Http\Controllers;

use App\Models\Salespersons;
use Illuminate\Http\Request;

class SalespersonsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Salespersons $salespersons)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Salespersons $salespersons)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Salespersons $salespersons)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Salespersons $salespersons)
    {
        //
    }
}
